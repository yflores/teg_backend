import pymysql


class DataBase:
    conn = None

    def connect(self):
        """Consulta las propiedades de conexión del archivo monitor.properties en la sección [DB]
        y crea la conexión a la base de datos. Esto se realiza una sola vez por cada instancia de la clase."""
        if self.conn is None:
            # config = configparser.ConfigParser()
            # if not config.read('monitor.properties'):
            #     raise Exception('Error leyendo el archivo monitor.properties')
            # self.conn = pyodbc.connect(config['DB']['connstring'])
            self.conn = pymysql.connect(host='localhost', user='root', password='1234', db='postgrado')

    def queryAll(self, sql: str, params: list=[], columns: list=None):
        """
        Ejecuta una consulta a la base de datos y devuelve todos los registros.

        :param sql: Comando SELECT a ejecutar.
        :param params: Lista de parámetros para asociar al comando SELECT.
        :param columns: Lista opcional de nombres de columnas para los registros consultados.\n
            Si no se especifica este parámetro los registros se devuelven con los nombres de columnas retornados por
            la consulta ejecutada.
        :return: Retorna una lista de diccionarios con los datos de cada registro retornado por la consulta ejecutada.\n
            Ej. [{"server_id": 1, "server_name": "DMZ", ...}, ...]
        """
        self.connect()
        cursor = self.conn.cursor()
        cursor.execute(sql, params)
        rows = cursor.fetchall()
        if columns is None:
            columns = [column[0].lower() for column in cursor.description]
        cursor.close()
        return [dict(zip(columns, row)) for row in rows]

    def queryOne(self, sql: str, params: list=[], columns: list=None):
        """
        Ejecuta una consulta a la base de datos y devuelve el primer registro.

        :param sql: Comando SELECT a ejecutar.
        :param params: Lista de parámetros para asociar al comando SELECT.
        :param columns: Lista opcional de nombres de columnas para los registros consultados.\n
            Si no se especifica este parámetro los registros se devuelven con los nombres de columnas retornados por
            la consulta ejecutada.
        :return: Retorna un diccionario con los datos del primer registro retornado por la consulta ejecutada.\n
            Ej. {"server_id": 1, "server_name": "DMZ", ...}
        """
        self.connect()
        cursor = self.conn.cursor()
        cursor.execute(sql, params)
        row = cursor.fetchone()
        if row is None:
            return None
        if columns is None:
            columns = [column[0].lower() for column in cursor.description]
        cursor.close()
        return dict(zip(columns, row))

    def insert(self, table: str, datos: dict=None, columns=None, values: list=None):
        """
        Inserta uno o varios registros en una tabla.

        :param table: Nombre de la tabla.
        :param datos: Diccionario con las keys para los nombres de columnas y los valores para insertar.\n
            Ej. {"SRV_ID": 1, "SRV_NOMBRE": "DMZ", ...}\n
            Este diccionario sobreescribe los valores de los parámetros columns y values.
        :param columns: Columnas de la tabla donde se van a insertar los datos.\n
            Puede ser un string separado por comas. ej. 'SRV_ID, SRV_NOMBRE, ...'\n
            Puede ser una lista de string. ej. ['SRV_ID', 'SRV_NOMBRE', ...]\n
        :param values: Lista de valores a insertar en la tabla.\n
            Puede ser una lista de valores simples para un solo registro. ej. [1, 'DMZ', ...]\n
            Puede ser una lista de tuplas para insertar varios registros. ej. [(1, 'DMZ', ...), (2, 'PROD', ...), ...]
        """
        self.connect()
        cursor = self.conn.cursor()

        if datos is not None:
            columns = []
            values = []
            for col, val in datos.items():
                columns.append(col)
                values.append(val)

        if isinstance(columns, str):
            columns = "("+columns+")"
        elif isinstance(columns, list):
            columns = "("+", ".join(columns)+")"

        if isinstance(values[0], (list, tuple)):
            marks = "?" + (",?" * (len(values[0]) - 1))
            cursor.executemany(f"insert into {table} {columns} values ({marks})", values)
        else:
            # marks = "?" + (",?" * (len(values) - 1))
            data = str(values[0])
            if len(values) > 1:
                i = 1
                while i < len(values):
                    data = data + ", " + str(values[i])
                    i = (i + 1)
                data = str(values)[1:(len(str(values)) - 1)]
                cursor.execute(f"insert into {table} {columns.upper()} VALUES ({data})")
            else:
                data = values[0]
                cursor.execute(f"insert into {table} {columns.upper()} VALUES ('{data}')")

        cursor.close()

    def insert_user_encrypt(self, table: str, datos: dict):
        self.connect()
        cursor = self.conn.cursor()
        cursor.execute(f"insert into {table} (USU_USUARIO, USU_CLAVE, USU_ROLE, FAC_ID, USU_DESCRIPCION) VALUES ('{datos['USU_USUARIO']}', {datos['USU_CLAVE']}, {datos['USU_ROLE']}, {datos['FAC_ID']}, '{datos['USU_DESCRIPCION']}')")
        cursor.close()

    def get_user_with_password_decrypt(self, table: str, attribute: str, columns: list=None):
        self.connect()
        cursor = self.conn.cursor()
        cursor.execute(f"SELECT USU_ID, USU_USUARIO, USU_CLAVE, USU_ROLE, FAC_ID, USU_DESCRIPCION FROM {table} WHERE USU_USUARIO = '{attribute}'")
        row = cursor.fetchone()
        if row is None:
            return None
        if columns is None:
            columns = [column[0].lower() for column in cursor.description]
        cursor.close()
        return dict(zip(columns, row))

    def update(self, table: str, datos: dict, where: dict):
        self.connect()
        cursor = self.conn.cursor()

        sql = f"update {table} set "
        values = []
        for col, val in datos.items():
            if val is not None:
                sql += f"{col} = '{val}', "
                values.append(val)

        sql = sql.rstrip(', ')
        sql += " where "
        for col, val in where.items():
            sql += f"{col} = {val} and "
            values.append(val)

        sql = sql.rstrip(' and ')
        cursor.execute(sql)
        cursor.close()

    def delete(self, table: str, where: str):
        self.connect()
        cursor = self.conn.cursor()

        sql = f"delete from {table} where {where}"
        cursor.execute(sql)
        cursor.close()

    def commit(self):
        self.conn.commit()

    def rollback(self):
        self.conn.rollback()

