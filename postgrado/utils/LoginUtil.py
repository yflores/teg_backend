import jwt


class LoginUtil:
    @staticmethod
    def encode_user(user, secret_key):
        """
        encode user payload as a jwt
        :param user:
        :return:
        """
        encoded_data = jwt.encode(payload={"name": user},
                                  key=secret_key,
                                  algorithm="HS256")

        return encoded_data
