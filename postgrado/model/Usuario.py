class Usuario:
    username = str(""),
    role = int(0),
    facultyId = int(0),
    description = str(""),
    token = str("")
    datetime_now = str("")

    def __init__(self, username, role, facultyId, description, token, datetime_now):
        self.username = username
        self.role = role
        self.facultyId = facultyId
        self.description = description
        self.token = token
        self.datetime_now = datetime_now
