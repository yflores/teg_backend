from datetime import datetime

from flask import make_response
from flask_restful import Resource, request, abort
from common.DataBase import DataBase
import simplejson as json
import ast
import random
from apscheduler.schedulers.background import BackgroundScheduler


# Clase para validar si el Usuario.py es permitido o no
from postgrado.model.Usuario import Usuario
from postgrado.utils.LoginUtil import LoginUtil


class Login(DataBase, Resource):
    columns = ['id', 'USU_USUARIO', 'USU_CLAVE', 'USU_ROLE', 'FAC_ID', 'USU_DESCRIPCION']
    representations = {'application/json': make_response}

    def post(self):
        data_front_end = ast.literal_eval(json.dumps(request.get_data().decode("utf-8")))
        parser = json.loads(data_front_end)
        login_user = {
            "USER": str(parser["username"]),
            "PASS": str(parser["password"])
        }

        result = self.get_user_with_password_decrypt("usuario", login_user["USER"], self.columns)
        data_user = -1
        if bool(result):
            if result["USU_USUARIO"] == login_user["USER"]:
                if result["USU_CLAVE"] == login_user["PASS"]:
                    datetime_now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                    token = LoginUtil.encode_user(result["USU_USUARIO"], 'postgrado')
                    print("Toke -->: " + token)
                    data_user = Usuario(result["USU_USUARIO"], result["USU_ROLE"], result["FAC_ID"], result["USU_DESCRIPCION"], token, datetime_now)
                    self.save_token_session(result['id'], token, datetime_now)
        else:
            data_user = Usuario('None', -1, 0, '', '', '')

        return json.dumps(data_user.__dict__), 201, {'Access-Control-Allow-Origin': '*'}

    def generate_pin_password(self):
        password_pin = ""
        i = 0
        while i <= 7:
            password_pin = password_pin + str(random.randrange(1, 9))
            i = (i + 1)
        return password_pin

    def save_token_session(self, usu_id, token, datetime_now):
        token_session = {
            'usu_id': usu_id,
            'tks_token': token,
            'tks_active': int(1),
            'tks_datetime': datetime_now
        }
        self.insert('tokensession', token_session)
        self.commit()


class TokenSession(DataBase, Resource):
    columns = ['tks_id', 'usu_id', 'tks_token', 'tks_active', 'datatime_now']
    representations = {'application/json': make_response}

    def get(self, usu_id):
        try:
            session_list = self.queryAll("SELECT * FROM tokensession", [], self.columns)

            response = dict()
            for session in session_list:
                if int(session['usu_id']) == int(usu_id) and int(session["tks_active"]) == int(1):
                    response = {
                        "tks_id": session["tks_id"],
                        "usu_id": session["usu_id"],
                        "tks_token": session["tks_token"],
                        "tks_active": session["tks_active"],
                        "datatime_now": str(session["datatime_now"])
                    }

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(response), 201, {'Access-Control-Allow-Origin': '*'}


class FacultyByUsername(DataBase, Resource):
    columns = ['id', 'USU_USUARIO', 'USU_CLAVE', 'USU_ROLE', 'FAC_ID', 'USU_DESCRIPCION']
    representations = {'application/json': make_response}

    def get(self, username):

        results = self.queryAll("SELECT * FROM usuario", [], self.columns)
        faculty_id = 0
        for user in results:
            if user["USU_USUARIO"] == str(username):
                faculty_id = int(user["FAC_ID"])

        return json.dumps(faculty_id), 201, {'Access-Control-Allow-Origin': '*'}


class DatetimeNowTokenUser(DataBase, Resource):

    representations = {'application/json': make_response}

    def get(self, usu_id):
        try:
            data = {
                "tks_datetime": datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            }
            self.update('TOKENSESSION', data,  {'usu_id': int(usu_id)})
            self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(data), 201, {'Access-Control-Allow-Origin': '*'}
