import ast

from flask import make_response
from flask_restful import Resource, abort, request
from common.DataBase import DataBase
import simplejson as json


class ItemsService(DataBase, Resource):
    columns = ['id', 'pre_id', 'data', 'itm_seleccion', 'itm_combo', 'itm_observacion', 'value']

    representations = {'application/json': make_response}

    def get(self):
        try:
            items = self.queryAll("SELECT * FROM item", [], self.columns)

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(items), 201, {'Access-Control-Allow-Origin': '*'}


class ItemDeleteArrayIdService(DataBase, Resource):
    columnsSeleccion = ['slm_id', 'itm_id', 'slm_valor', 'slm_response']

    representations = {'application/json': make_response}

    def post(self):
        try:
            parser = ast.literal_eval(request.get_data().decode("utf-8"))

            for id_itm in parser:

                # Agregar aqui que elimine Los elementos de la seleccion multiple
                seleccionMultiple = self.queryAll(f"SELECT * FROM SELECCIONMULTIPLE WHERE ITM_ID = {int(id_itm)} ", [], self.columnsSeleccion)

                if seleccionMultiple.__len__() > 0:
                    self.delete("SELECCIONMULTIPLE", f'ITM_ID = {id_itm}')
                    self.commit()

                self.delete("ITEM", f'ITM_ID = {id_itm}')
                self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(''), 201, {'Access-Control-Allow-Origin': '*'}


class ItemService(DataBase, Resource):
    columns = ['itm_id', 'pre_id', 'itm_data', 'itm_seleccion', 'itm_combo', 'itm_observacion', 'itm_value', 'itm_fecha']

    representations = {'application/json': make_response}

    def get(self, pre_id):
        try:
            items = self.queryAll("SELECT * FROM item", [], self.columns)

            items_pregunta = []
            for itm in items:
                if str(itm["pre_id"]) == pre_id:
                    items_pregunta.append(itm)

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(items_pregunta), 201, {'Access-Control-Allow-Origin': '*'}
