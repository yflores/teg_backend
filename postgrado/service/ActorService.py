from flask import make_response
from flask_restful import Resource, abort, request
from common.DataBase import DataBase
import simplejson as json
import ast

# Clase para validar si el Usuario.py es permitido o no
class ActorService(DataBase, Resource):
    columns = ['id', 'nombre', 'edad', 'cedula', 'fechanac']

    representations = {'application/json': make_response}

    def get(self, id_user):
        try:
            actors = self.queryAll("SELECT * FROM actor", [], self.columns)

            for actor in actors:
                if actor["id"] == int(id_user):
                    actor_enc = {
                        "id": actor["id"],
                        "nombre": actor["nombre"],
                        "edad": actor["edad"],
                        "cedula": actor["cedula"],
                        "fechanac": actor["fechanac"]
                    }

            print(actor_enc)

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(actor_enc), 201, {'Access-Control-Allow-Origin': '*'}