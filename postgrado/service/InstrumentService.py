from flask import make_response
from flask_restful import Resource, abort, request
from common.DataBase import DataBase
import simplejson as json
import ast

from common.core import Core
from postgrado.service.FacultadService import FacultadServiceByOne
from postgrado.service.SecurityService import Login


class InstrumentsService(DataBase, Resource):
    columns = ['id', 'codigo', 'iniciales', 'nombre', 'descripcion', 'profesor', 'participante',
               'egresado', 'empleado']

    representations = {'application/json': make_response}

    def get(self):
        try:
            instruments = self.queryAll("SELECT * FROM instrumento", [], self.columns)

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(instruments), 201, {'Access-Control-Allow-Origin': '*'}


class InstrumentSaveService(DataBase, Resource):
    representations = {'application/json': make_response}

    def post(self):
        try:
            parser = ast.literal_eval(request.get_data().decode("utf-8"))
            data = {
                "INT_CODIGO": parser["int_codigo"],
                "INT_INICIALES": parser["int_iniciales"],
                "INT_NOMBRE": parser["int_nombre"],
                "INT_DESCRIPCION": parser["int_descripcion"],
                # "INT_LISTA_MATRIZ": parser["int_lista_matriz"],
                "INT_LISTA_MATRIZ": int(0), #Se coloco 0 para que fuera de tipo lista la evaluación por defecto.
                "INT_PROFESOR": parser["int_profesor"],
                "INT_PARTICIPANTE": parser["int_participante"],
                "INT_EGRESADO": parser["int_egresado"],
                "INT_EMPLEADO": parser["int_empleado"],
                "INT_PROGRAMA": int(0),
                "FAC_ID": parser["fac_id"]
            }
            self.insert('INSTRUMENTO', data)
            self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(''), 201, {'Access-Control-Allow-Origin': '*'}


class InstrumentUpdateService(DataBase, Resource):
    representations = {'application/json': make_response}

    def post(self):
        try:
            parser = ast.literal_eval(request.get_data().decode("utf-8"))
            data = {
                "INT_CODIGO": parser["int_codigo"],
                "INT_INICIALES": parser["int_iniciales"],
                "INT_NOMBRE": parser["int_nombre"],
                "INT_DESCRIPCION": parser["int_descripcion"],
                # "INT_LISTA_MATRIZ": parser["int_lista_matriz"],
                "INT_LISTA_MATRIZ": int(0),  # Se coloco 0 para que fuera de tipo lista la evaluación por defecto.
                "INT_PROFESOR": parser["int_profesor"],
                "INT_PARTICIPANTE": parser["int_participante"],
                "INT_EGRESADO": parser["int_egresado"],
                "INT_EMPLEADO": parser["int_empleado"],
                "INT_PROGRAMA": int(0),
                "FAC_ID": parser["fac_id"]
            }
            self.update('INSTRUMENTO', data,  {'INT_ID': int(parser["id"])})
            self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(''), 201, {'Access-Control-Allow-Origin': '*'}

class InstrumentService(DataBase, Resource):
    columns = ['id', 'int_codigo', 'int_iniciales', 'int_nombre', 'int_descripcion', 'int_profesor', 'int_participante',
               'int_egresado', 'int_empleado']

    representations = {'application/json': make_response}

    def get(self, code):
        try:
            instruments = self.queryAll("SELECT * FROM instrumento", [], self.columns)

            instrument_selected = ""
            for intrument in instruments:
                if str(intrument["int_codigo"]).upper() == str(code).upper():
                    instrument_selected = intrument

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(instrument_selected), 201, {'Access-Control-Allow-Origin': '*'}


class InstrumentsServiceByFacultyId(DataBase, Resource):
    columns = ['id', 'codigo', 'iniciales', 'nombre', 'descripcion', 'profesor', 'participante',
               'egresado', 'empleado', 'programa', 'fac_id']

    representations = {'application/json': make_response}

    def get(self, facultyId):
        try:
            instruments = self.queryAll("SELECT * FROM instrumento", [], self.columns)

            instruments_faculty = []
            for int in instruments:
                if str(int["fac_id"]) == facultyId:
                    instruments_faculty.append(int)

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(instruments_faculty), 201, {'Access-Control-Allow-Origin': '*'}


class DataActorEvaluation(DataBase, Resource):
    columns = ['id', 'actor', 'code_instrument', 'names', 'username', 'key', 'email']

    representations = {'application/json': make_response}

    def post(self):
        try:
            parser = ast.literal_eval(request.get_data().decode("utf-8"))
            data = {
                "DAE_ACTOR": parser["actor"],
                "DAE_CODEINSTRUMENT": parser["code_instrument"],
                "DAE_FIRST_NAME": str(parser["primer_nombre"]) + str(" ") + str(parser["primer_apellido"]),
                "DAE_USERNAME": str(""),
                "DAE_IDENTIFICATION": parser["identification"],
                "DAE_EMAIL": parser["email"],
                "DAE_ACTIVE": 1,
                "DAE_PERIODO": str(parser["periodo"])
                # "PRO_ID": parser["pro_id"]
            }
            dataUsername = ""
            dataUsername = dataUsername + str(parser["primer_nombre"][0]).lower()
            dataUsername = dataUsername + str(parser["primer_apellido"]).lower()

            faculty = FacultadServiceByOne.get(FacultadServiceByOne(), id_facultad = parser["faculty_id"])

            dataUser = {
                "USU_USUARIO": dataUsername,
                "USU_CLAVE": Login.generate_pin_password(self),
                "USU_ROLE": int(2),
                "FAC_ID": int(parser["faculty_id"]),
                "USU_DESCRIPCION": f"Usuario de la facultad de {ast.literal_eval(str(faculty[0]))['fac_nombre']}"
            }

            valueUser = 0
            bandera = True
            columns = ['id', 'Usuario.py', 'clave', 'role']
            users = self.queryAll("SELECT * FROM USUARIO", [], columns)

            encontrado = False
            dataUsername = dataUser['USU_USUARIO']
            while bandera or not(encontrado):
                valueUser = valueUser + 1
                encontrado = False
                for user in users:
                    if (user['Usuario.py'] == dataUser['USU_USUARIO']):
                        encontrado = True
                        break

                if (encontrado):
                    dataUser['USU_USUARIO'] = dataUsername + str(valueUser)
                else:
                    bandera = False
                    encontrado = True
            self.insert_user_encrypt('USUARIO', dataUser)
            self.commit()

            data["DAE_USERNAME"] = dataUser["USU_USUARIO"]
            self.insert('DATAACTOREVALUATION', data)
            self.commit()

            attributes = {
                "SUBJECT": 'Asignación de Evaluacion y de Credenciales de Acceso',
                "MESSAGE": f'<a>Estimado, sus datos de acceso en el Sistema de Autoevaluación '
                           f'son:</a><br><a>{dataUser["USU_USUARIO"]}</a><br>'
                           f'<a>{dataUser["USU_CLAVE"]}</a>'
            }

            Core.send_email(str(data["DAE_EMAIL"]), attributes)

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(''), 201, {'Access-Control-Allow-Origin': '*'}


class GetDataActorEvaluation(DataBase, Resource):
    columns = ['id', 'code_instrument', 'names', 'username', 'identification', 'email', 'active', 'actor', 'periodo']

    representations = {'application/json': make_response}

    def get(self, username):
        try:
            instruments = self.queryAll("SELECT * FROM DATAACTOREVALUATION", [], self.columns)

            list_assigment = []
            for int in instruments:
                if int["username"] == username and int["active"] == 1:
                    list_assigment.append(int)

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(list_assigment), 201, {'Access-Control-Allow-Origin': '*'}


class DataActorEvaluationCheck(DataBase, Resource):
    columns = ['id', 'code_instrument', 'names', 'username', 'identification', 'email', 'active', 'actor', 'periodo']

    representations = {'application/json': make_response}

    def post(self):
        try:
            parser = ast.literal_eval(request.get_data().decode("utf-8"))
            instruments = self.queryAll("SELECT * FROM DATAACTOREVALUATION", [], self.columns)
            for instrument in instruments:
                if instrument["username"] == parser["username"] and str(instrument["code_instrument"]).upper() == str(parser["codigo"]).upper():
                    data = {
                        "DAE_ACTOR": instrument["actor"],
                        "DAE_CODEINSTRUMENT": instrument["code_instrument"],
                        "DAE_FIRST_NAME": instrument["names"],
                        "DAE_USERNAME": instrument["username"],
                        "DAE_IDENTIFICATION": instrument["identification"],
                        "DAE_EMAIL": instrument["email"],
                        "DAE_ACTIVE": 0,
                        "DAE_PERIODO": str(instrument["periodo"])
                    }
                    self.delete('DATAACTOREVALUATION', f'DAE_ID = {instrument["id"]}')
                    self.commit()
                    self.insert('DATAACTOREVALUATION', data)
                    self.commit()
                    del data["DAE_ACTIVE"]
                    data = {
                        "CHECK_ACTOR": instrument["actor"],
                        "CHECK_CODEINSTRUMENT": instrument["code_instrument"],
                        "CHECK_NAMES": instrument["names"],
                        "CHECK_USERNAME": instrument["username"],
                        "CHECK_IDENTIFICATION": instrument["identification"],
                        "CHECK_EMAIL": instrument["email"],
                        # "PRO_ID": parser["prod_id"]
                        "PRO_ID": int(1),
                        "CHECK_PERIODO": str(instrument["periodo"])
                    }
                    self.insert('EVALCHECK', data)
                    self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(''), 201, {'Access-Control-Allow-Origin': '*'}

class DeleteInstrumentService(DataBase, Resource):
    columns_instrument = ['id', 'codigo', 'iniciales', 'nombre', 'descripcion', 'profesor', 'participante',
                          'egresado', 'empleado', 'programa', 'fac_id']
    columns_evaluacion = ['id', 'code_instrument', 'names', 'username', 'key', 'email']
    columns_pregunta = ['pre_id', 'eval_id', 'pregunta', 'pre_value']
    columns_item = ['id', 'pre_id', 'data', 'itm_seleccion', 'itm_combo', 'itm_observacion', 'value']

    representations = {'application/json': make_response}

    def get(self, int_id):
        try:
            instruments = self.queryAll("SELECT * FROM instrumento", [], self.columns_instrument)
            for instrument in instruments:
                if str(instrument["id"]) == str(int_id):
                    evaluacions = self.queryAll("SELECT * FROM evaluacion", [], self.columns_evaluacion)
                    for evaluacion in evaluacions:
                        if str(evaluacion["code_instrument"]) == str(instrument["codigo"]):
                            preguntas = self.queryAll("SELECT * FROM pregunta", [], self.columns_pregunta)
                            for pregunta in preguntas:
                                if str(pregunta["eval_id"]) == str(evaluacion["id"]):
                                    items = self.queryAll("SELECT * FROM item", [], self.columns_item)
                                    for item in items:
                                        if str(item["pre_id"]) == str(pregunta["pre_id"]):
                                            self.delete('ITEM', f'ITM_ID = {int(item["id"])}')
                                            self.commit()
                                    self.delete('PREGUNTA', f'PRE_ID = {int(pregunta["pre_id"])}')
                                    self.commit()
                            self.delete('EVALUACION', f'EVAL_ID = {int(evaluacion["id"])}')
                            self.commit()
                            break
                    self.delete('INSTRUMENTO', f'INT_ID = {int(instrument["id"])}')
                    self.commit()
                    break

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(''), 201, {'Access-Control-Allow-Origin': '*'}

class InstrumentsServiceById(DataBase, Resource):
    columns = ['id', 'int_codigo', 'int_iniciales', 'int_nombre', 'int_descripcion', 'int_profesor', 'int_participante',
               'int_egresado', 'int_empleado']

    representations = {'application/json': make_response}

    def get(self, int_id):
        try:
            instruments = self.queryAll("SELECT * FROM instrumento", [], self.columns)

            instrument = ""
            for int in instruments:
                if str(int["id"]) == int_id:
                    instrument = int
                    break

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(instrument), 201, {'Access-Control-Allow-Origin': '*'}
