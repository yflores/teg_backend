from flask import make_response
from flask_restful import Resource, abort, request
from common.DataBase import DataBase
import simplejson as json
import ast


class ProgramByFacultadService(DataBase, Resource):
    columns = ['pro_id', 'pro_nombre', 'fac_id', 'pro_descripcion']

    representations = {'application/json': make_response}

    def get(self, id_facultad):
        try:
            programs = self.queryAll("SELECT * FROM programa", [], self.columns)

            programsByFacultadId = []
            for program in programs:
                if str(program["fac_id"]) == str(id_facultad):
                    programsByFacultadId.append(program)

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(programsByFacultadId), 201, {'Access-Control-Allow-Origin': '*'}


class ProgramSaveUpdateService(DataBase, Resource):
    representations = {'application/json': make_response}

    def post(self):
        try:
            parser = ast.literal_eval(request.get_data().decode("utf-8"))
            data = {
                "pro_nombre": parser["pro_nombre"],
                "fac_id": int(parser["fac_id"]),
                "pro_descripcion": parser["pro_descripcion"]
            }
            if parser["pro_id"] != 0:
                self.update('PROGRAMA', data, {'pro_id': int(parser["pro_id"])})
            else:
                self.insert('PROGRAMA', data)
            self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(''), 201, {'Access-Control-Allow-Origin': '*'}


class ProgramDeleteService(DataBase, Resource):
    representations = {'application/json': make_response}

    def post(self):
        try:
            parser = ast.literal_eval(request.get_data().decode("utf-8"))
            self.delete('PROGRAMA', f'pro_id = {int(parser["pro_id"])}')
            self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(''), 201, {'Access-Control-Allow-Origin': '*'}


class ProgramServiceByOne(DataBase, Resource):
    columns = ['pro_id', 'pro_nombre', 'fac_id',  'pro_descripcion']

    representations = {'application/json': make_response}

    def get(self, pro_id):
        try:
            programs = self.queryAll("SELECT * FROM programa", [], self.columns)

            for program in programs:
                if str(program["pro_id"]) == str(pro_id):
                    programOut = program

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(programOut), 201, {'Access-Control-Allow-Origin': '*'}

