from flask import make_response
from flask_restful import Resource, abort, request
from common.DataBase import DataBase
import simplejson as json
import ast


class UserByFacultadIdService(DataBase, Resource):
    columns = ['id', 'username', 'password', 'role', 'fac_id', 'descripcion']

    representations = {'application/json': make_response}

    def get(self, id_facultad):
        try:
            users = self.queryAll("SELECT * FROM usuario", [], self.columns)

            userByFacultadId = []
            for user in users:
                if str(user["fac_id"]) == str(id_facultad):
                    userByFacultadId.append(user)

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(userByFacultadId), 201, {'Access-Control-Allow-Origin': '*'}


class UserByUsernameService(DataBase, Resource):
    columns = ['id', 'username', 'password', 'role', 'fac_id', 'descripcion']

    representations = {'application/json': make_response}

    def get(self, username):
        try:
            users = self.queryAll("SELECT * FROM usuario", [], self.columns)

            userByUsername = []
            for user in users:
                if str(user["username"]) == str(username):
                    userByUsername.append(user)
                    break

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(userByUsername[0]), 201, {'Access-Control-Allow-Origin': '*'}

class UserSaveUpdateService(DataBase, Resource):
    columns = ['id', 'username', 'password', 'role', 'fac_id', 'descripcion']

    representations = {'application/json': make_response}

    def post(self):
        try:
            parser = ast.literal_eval(request.get_data().decode("utf-8"))
            data = {
                "usu_usuario": parser["username"],
                "usu_clave": parser["password"],
                "usu_role": int(parser["role"]),
                "fac_id": int(parser["fac_id"]),
                "usu_descripcion": parser["descripcion"]
            }
            if parser["id"] != 0:
                self.update('USUARIO', data, {'usu_id': int(parser["id"])})
            else:
                self.insert('USUARIO', data)
            self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(''), 201, {'Access-Control-Allow-Origin': '*'}


class UserDeleteService(DataBase, Resource):
    representations = {'application/json': make_response}

    def post(self):
        try:
            parser = ast.literal_eval(request.get_data().decode("utf-8"))
            self.delete('USUARIO', f'usu_id = {int(parser["id"])}')
            self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(''), 201, {'Access-Control-Allow-Origin': '*'}
