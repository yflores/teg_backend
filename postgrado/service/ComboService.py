import ast

from flask import make_response
from flask_restful import Resource, abort, request
from common.DataBase import DataBase
import simplejson as json


class CombosService(DataBase, Resource):
    columns = ['cmb_id', 'cmb_titulo']

    representations = {'application/json': make_response}

    def get(self):
        try:
            combos = self.queryAll("SELECT * FROM combos", [], self.columns)

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(combos), 201, {'Access-Control-Allow-Origin': '*'}


class ComboServiceId(DataBase, Resource):
    columns = ['cmb_id', 'cmb_titulo']

    representations = {'application/json': make_response}

    def get(self, id):
        try:
            combos = self.queryAll("SELECT * FROM combos", [], self.columns)

            comboEncontrado = str("")
            for combo in combos:
                if str(combo["cmb_id"]) == str(id):
                    comboEncontrado = combo
                    break

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(comboEncontrado['cmb_titulo']), 201, {'Access-Control-Allow-Origin': '*'}


class ComboService(DataBase, Resource):
    columns = ['cmb_id', 'cmb_titulo']

    representations = {'application/json': make_response}

    def get(self, name_combo):
        try:
            combos = self.queryAll("SELECT * FROM combos", [], self.columns)

            for combo in combos:
                if combo["cmb_titulo"] == name_combo:
                    comboEncontrado = combo
                    break

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(comboEncontrado), 201, {'Access-Control-Allow-Origin': '*'}


class ComboServicePost(DataBase, Resource):
    columns = ['cmb_id', 'cmb_titulo']

    representations = {'application/json': make_response}

    def post(self):
        try:
            parser = ast.literal_eval(request.get_data().decode("utf-8"))
            name_split = parser["cmb_titulo"].split(str(" "))
            name_definitive = str("")
            for data in name_split:
                name_definitive += data + str("_")
            name_definitive = name_definitive[:-1]
            parser["cmb_titulo"] = name_definitive
            out = self.insert('COMBOS', parser)
            self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(parser), 201, {'Access-Control-Allow-Origin': '*'}


class ItemComboService(DataBase, Resource):
    columns = ['itcm_id', 'cmb_id', 'cmb_valor']

    representations = {'application/json': make_response}

    def post(self):
        try:
            parser = ast.literal_eval(request.get_data().decode("utf-8"))
            if "cmb_valor" in parser:
                itemCombo = {
                    "CMB_ID": str(parser["cmb_id"]),
                    "cmb_valor": str(parser["cmb_valor"])
                }
                self.insert('ITEMCOMBO', itemCombo)
                self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(1), 201, {'Access-Control-Allow-Origin': '*'}


class ItemsComboService(DataBase, Resource):
    columns = ['itcm_id', 'cmb_id', 'cmb_valor']

    representations = {'application/json': make_response}

    def get(self, id_combo):
        itm_combos = self.queryAll("SELECT * FROM itemcombo", [], self.columns)

        itm_values = []
        for itm_combo in itm_combos:
            if str(itm_combo["cmb_id"]) == id_combo:
                itm_values.append(itm_combo)

        return json.dumps(itm_values), 201, {'Access-Control-Allow-Origin': '*'}
