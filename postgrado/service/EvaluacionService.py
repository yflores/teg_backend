from flask import make_response
from flask_restful import Resource, abort, request
from common.DataBase import DataBase
import simplejson as json
import ast


class EvaluacionsService(DataBase, Resource):
    columns = ['eval_id', 'int_codigo', 'programa', 'condicion_informante', 'tutor', 'docente_condicion',
               'criterio_escogencia']

    representations = {'application/json': make_response}

    def get(self):
        try:
            evaluacions = self.queryAll("SELECT * FROM evaluacion", [], self.columns)

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(evaluacions), 201, {'Access-Control-Allow-Origin': '*'}

    def post(self):
        try:
            parser = ast.literal_eval(request.get_data().decode("utf-8"))
            data = {
                "INT_CODIGO": parser["int_codigo"],
                "EVAL_PROGRAMA": parser["eval_programa"]#,
                #"EVAL_CONDICIONINFORMANTE": str(""),
                #"EVAL_TUTOR": str(""),
                #"EVAL_DOCENTEUNICONDICION": str(""),
                #"EVAL_CRITERIOSESCOGENCIA": str("")

            }
            self.insert('EVALUACION', data)
            self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(''), 201, {'Access-Control-Allow-Origin': '*'}


class EvaluacionUpdateService(DataBase, Resource):

    representations = {'application/json': make_response}

    def post(self):
        try:
            parser = ast.literal_eval(request.get_data().decode("utf-8"))
            data = {
                "INT_CODIGO": parser["int_codigo"]#,
                #"EVAL_PROGRAMA": parser["eval_programa"],
                #"EVAL_CONDICIONINFORMANTE": str(""),
                #"EVAL_TUTOR": str(""),
                #"EVAL_DOCENTEUNICONDICION": str(""),
                #"EVAL_CRITERIOSESCOGENCIA": str("")

            }
            self.update('EVALUACION', data,  {'EVAL_ID': int(parser["eval_id"])})
            self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(''), 201, {'Access-Control-Allow-Origin': '*'}


class EvaluacionService(DataBase, Resource):
    columns = ['eval_id', 'int_codigo', 'programa']

    representations = {'application/json': make_response}

    def get(self, code_int):
        try:
            evaluacions = self.queryAll("SELECT * FROM evaluacion", [], self.columns)

            evaluacion = ""
            for eval in evaluacions:
                if eval["int_codigo"] == code_int:
                    evaluacion = eval

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(evaluacion), 201, {'Access-Control-Allow-Origin': '*'}


class EvaluacionCheckService(DataBase, Resource):
    columns = ['id', 'actor', 'code_instrument', 'names', 'username', 'identification', 'email', 'pro_id', 'periodo', 'pro_name']

    representations = {'application/json': make_response}

    def get(self):
        try:
            sql = "SELECT evk.CHECK_ID as id, evk.CHECK_ACTOR as actor, evk.CHECK_CODEINSTRUMENT as code_instrument, " \
                  "evk.CHECK_NAMES as names, evk.CHECK_USERNAME as username, evk.CHECK_IDENTIFICATION " \
                  "as identification, evk.CHECK_EMAIL as email, evk.PRO_ID as pro_id, evk.CHECK_PERIODO as periodo, " \
                  "p.PRO_NOMBRE FROM evalcheck evk inner join programa p on (evk.PRO_ID = p.PRO_ID)"

            evaluacions = self.queryAll(sql, [], self.columns)
            for eval in evaluacions:
                eval["evalConcatUser"] = eval["code_instrument"] + "_" + eval["username"]

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(evaluacions), 201, {'Access-Control-Allow-Origin': '*'}


class EvaluacionCheckByUsernameService(DataBase, Resource):
    columns = ['id', 'actor', 'code_instrument', 'names', 'username', 'key', 'email', 'pro_id']

    representations = {'application/json': make_response}

    def get(self, username):
        try:
            evaluacions = self.queryAll("SELECT * FROM evalcheck", [], self.columns)
            evaluacion = ""
            for eval in evaluacions:
                if eval["username"] == username:
                    evaluacion = eval

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(evaluacion), 201, {'Access-Control-Allow-Origin': '*'}


class EvaluationCheckByFilterService(DataBase, Resource):
    columns = ['id', 'actor', 'code_instrument', 'names', 'username', 'key', 'email', 'pro_id', 'periodo', 'pro_name']

    representations = {'application/json': make_response}

    def get(self, dataFilter: str):
        try:
            data = dataFilter.split("&")
            query = f'SELECT evk.CHECK_ID as id, evk.CHECK_ACTOR as actor, evk.CHECK_CODEINSTRUMENT as code_instrument, ' \
                    f'evk.CHECK_NAMES as names, evk.CHECK_USERNAME as username, evk.CHECK_IDENTIFICATION ' \
                    f'as identification, evk.CHECK_EMAIL as email, evk.PRO_ID as pro_id, evk.CHECK_PERIODO as periodo, ' \
                    f'p.PRO_NOMBRE FROM evalcheck evk inner join programa p on (evk.PRO_ID = p.PRO_ID) WHERE 1 = 1 '

            if data[0] != 'empty':
                query += f'AND evk.CHECK_ACTOR = \'{data[0]}\' '

            if data[1] != 'empty':
                query += f'AND evk.CHECK_PERIODO = \'{data[1]}\' '

            if data[2] != 'empty':
                query += f'AND evk.PRO_ID = \'{data[2]}\' '

            evaluacions = self.queryAll(query, [], self.columns)

            for eval in evaluacions:
                eval["evalConcatUser"] = eval["code_instrument"] + "_" + eval["username"]

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(evaluacions), 201, {'Access-Control-Allow-Origin': '*'}