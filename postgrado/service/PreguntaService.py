from flask import make_response
from flask_restful import Resource, abort, request
from common.DataBase import DataBase
import simplejson as json
import ast

class PreguntasService(DataBase, Resource):
    columns = ['pre_id', 'eval_id', 'pregunta', 'pre_value', 'ind_variable']

    representations = {'application/json': make_response}

    def get(self):
        try:
            preguntas = self.queryAll("SELECT * FROM pregunta", [], self.columns)

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(preguntas), 201, {'Access-Control-Allow-Origin': '*'}

    def post(self):
        try:
            parser = ast.literal_eval(request.get_data().decode("utf-8"))

            for pregunta in parser:
                data = {
                    "EVAL_ID": pregunta["eval_id"],
                    "PRE_PREGUNTA": pregunta["pre_pregunta"],
                    "PRE_VALUE": pregunta["pre_value"] if "pre_value" in pregunta else str(''),
                    "IND_VARIABLE": pregunta["ind_variable"]
                }
                if int(pregunta["pre_id"]) != 0:
                    data["PRE_ID"] = pregunta["pre_id"]
                    self.update('PREGUNTA', data, {'PRE_ID': int(data["PRE_ID"])})
                else:
                    self.insert('PREGUNTA', data)

                self.commit()

                preguntas = self.queryAll("SELECT * FROM pregunta", [], self.columns)
                for element in preguntas:
                    if element["eval_id"] == data["EVAL_ID"] and element["pregunta"] == data["PRE_PREGUNTA"]:
                        pregunta_id = element["pre_id"]

                for element_item in pregunta["pre_sub_pregunta"]:
                    data = element_item["itm_data"]
                    if element_item["itm_data"] is None:
                        data = element_item["itm_value_seleccion"]

                    data_item_pregunta = {
                        "PRE_ID": pregunta_id,
                        "ITM_DATA": data,
                        "ITM_SELECCION": element_item["itm_seleccion"] if "itm_seleccion" in element_item else int(),
                        "ITM_COMBO": element_item["num_combo"] if "num_combo" in element_item else int(),
                        "ITM_OBSERVACION": element_item["itm_observacion"] if "itm_observacion" in element_item else int(),
                        "ITM_VALUE": str(""),
                        "ITM_FECHA": element_item["itm_fecha"] if "itm_fecha" in element_item else int()
                    }

                    if int(element_item["itm_id"]) != 0:
                        data_item_pregunta["ITM_ID"] = element_item["itm_id"]
                        self.update('ITEM', data_item_pregunta, {'PRE_ID': int(data_item_pregunta["ITM_ID"])})
                    else:
                        self.insert('ITEM', data_item_pregunta)

                    self.commit()

                    if int(data_item_pregunta["ITM_OBSERVACION"]) == 1:
                        list_element_selection = list(element_item["itm_value_seleccion"])
                        if len(list_element_selection) > 0:
                            id_inserted = self.queryAll("SELECT ITM_ID FROM ITEM ORDER BY ITM_ID DESC LIMIT 1;", [], ["ITM_ID"])
                            for element in list_element_selection:
                                data_selection_multiple = {
                                    "ITM_ID": int(id_inserted[0]["ITM_ID"]),
                                    "SLM_VALOR": element["slt_valor"] if "slt_valor" in element else str('')
                                }

                                if "slm_id" in element:
                                    self.update('SELECCIONMULTIPLE', data_selection_multiple, {'SLM_ID': int(element["slm_id"])})
                                else:
                                    self.insert('SELECCIONMULTIPLE', data_selection_multiple)

                                self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(''), 201, {'Access-Control-Allow-Origin': '*'}


class PreguntaService(DataBase, Resource):
    columns = ['pre_id', 'eval_id', 'pre_pregunta', 'pre_value','ind_variable']

    representations = {'application/json': make_response}

    def get(self, id_int):
        try:
            preguntas = self.queryAll("SELECT * FROM pregunta", [], self.columns)

            items = []
            for pre in preguntas:
                if str(pre["eval_id"]) == id_int:
                    items.append(pre)

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(items), 201, {'Access-Control-Allow-Origin': '*'}


class PreguntaDeleteArrayIdService(DataBase, Resource):

    representations = {'application/json': make_response}

    def post(self):
        try:
            parser = ast.literal_eval(request.get_data().decode("utf-8"))

            for id_int in parser:
                self.delete("PREGUNTA", f'PRE_ID = {int(id_int)}')
                self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(''), 201, {'Access-Control-Allow-Origin': '*'}


class PreguntasCheckService(DataBase, Resource):
    columns = ['pre_id', 'eval_id', 'pregunta', 'pre_value', 'ind_variable']

    representations = {'application/json': make_response}

    def post(self, username):
        try:
            parser = json.loads(request.get_data().decode("utf-8"))
            for indicator in parser:
                data = {
                    "EVAL_ID": indicator["eval_id"],
                    "PCR_PREGUNTA": indicator["pre_pregunta"],
                    "PCR_VALUE": indicator["pre_value"],
                    "PCR_USER": username,
                    "PCR_VARIABLE": indicator["ind_variable"]
                }
                if "pre_sub_pregunta" in indicator:
                    data["PCR_VALUE"] = str(indicator["pre_sub_pregunta"])
                self.insert('PREGUNTACHECK', data)
                self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(''), 201, {'Access-Control-Allow-Origin': '*'}


#Falta este metodo....
class PreguntasCheckByUserAndEval(DataBase, Resource):
    columns = ['pre_id', 'eval_id', 'pregunta', 'pre_value']

    representations = {'application/json': make_response}

    def post(self):
        try:
            columnInt = ['id', 'eval_id', 'pre_pregunta', 'value', 'username', 'ind_variable']
            parser = ast.literal_eval(request.get_data().decode("utf-8"))
            preguntas = self.queryAll("SELECT * FROM PREGUNTACHECK", [], columnInt)
            
            preguntasByUsername = []
            for pregunta in preguntas:
                if pregunta["username"] == parser["username"]:
                    preguntasByUsername.append(pregunta)
                    
            preguntasByUsernameAndEval = []
            for data in preguntasByUsername:
                if data["eval_id"] == parser["eval_id"]:
                    preguntasByUsernameAndEval.append(data)

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(preguntasByUsernameAndEval), 201, {'Access-Control-Allow-Origin': '*'}


class CantidadPreguntasByEvaluationId(DataBase, Resource):
    columns = ['pre_id', 'eval_id', 'pregunta', 'pre_value', 'ind_variable']

    representations = {'application/json': make_response}

    def get(self, id_evaluation):
        try:
            preguntas = self.queryAll("SELECT * FROM pregunta", [], self.columns)

            cant_preguntas = 0
            for pre in preguntas:
                if str(pre["eval_id"]) == id_evaluation:
                    cant_preguntas = cant_preguntas + 1

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(cant_preguntas), 201, {'Access-Control-Allow-Origin': '*'}
