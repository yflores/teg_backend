from flask import make_response
from flask_restful import Resource, abort, request
from common.DataBase import DataBase
import simplejson as json
import ast


class FacultadService(DataBase, Resource):
    columns = ['fac_id', 'fac_nombre', 'fac_descripcion']

    representations = {'application/json': make_response}

    def get(self):
        try:
            facultades = self.queryAll("SELECT * FROM facultad", [], self.columns)

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(facultades), 201, {'Access-Control-Allow-Origin': '*'}


class FacultadServiceByOne(DataBase, Resource):
    columns = ['fac_id', 'fac_nombre', 'fac_descripcion']

    representations = {'application/json': make_response}

    def get(self, id_facultad):
        try:
            facultades = self.queryAll("SELECT * FROM facultad", [], self.columns)

            for facultad in facultades:
                if str(facultad["fac_id"]) == str(id_facultad):
                    facultadOut = facultad

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(facultadOut), 201, {'Access-Control-Allow-Origin': '*'}


class FacultadSaveUpdateService(DataBase, Resource):
    representations = {'application/json': make_response}

    def post(self):
        try:
            parser = ast.literal_eval(request.get_data().decode("utf-8"))
            data = {
                "fac_nombre": parser["fac_nombre"],
                "fac_descripcion": parser["fac_descripcion"]
            }
            if parser["fac_id"] != 0:
                self.update('FACULTAD', data, {'fac_id': int(parser["fac_id"])})
            else:
                self.insert('FACULTAD', data)
            self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(''), 201, {'Access-Control-Allow-Origin': '*'}


class FacultadDeleteService(DataBase, Resource):
    representations = {'application/json': make_response}

    def post(self):
        try:
            parser = ast.literal_eval(request.get_data().decode("utf-8"))
            self.delete('FACULTAD', f'fac_id = {int(parser["fac_id"])}')
            self.commit()

        except Exception as e:
            abort(500, message="{0}: {1}".format(e.__class__.__name__, e.__str__()))

        return json.dumps(''), 201, {'Access-Control-Allow-Origin': '*'}
