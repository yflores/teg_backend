from flask import Flask
from flask_restful import Api
from flask_cors import CORS

#Postgrado
from postgrado.service.ProgramService import ProgramByFacultadService, ProgramSaveUpdateService, ProgramDeleteService, \
    ProgramServiceByOne
from postgrado.service.SecurityService import Login, FacultyByUsername, TokenSession, DatetimeNowTokenUser
from postgrado.service.ComboService import ComboService, ComboServiceId, ItemsComboService, CombosService, ComboServicePost, ItemComboService
from postgrado.service.InstrumentService import InstrumentsService, InstrumentService, InstrumentsServiceByFacultyId, \
    InstrumentSaveService, DataActorEvaluation, GetDataActorEvaluation, DataActorEvaluationCheck, \
    DeleteInstrumentService, InstrumentsServiceById, InstrumentUpdateService
from postgrado.service.ActorService import ActorService
from postgrado.service.EvaluacionService import EvaluacionsService, EvaluacionService, EvaluacionCheckService, \
    EvaluacionCheckByUsernameService, EvaluacionUpdateService, EvaluationCheckByFilterService
from postgrado.service.PreguntaService import PreguntasService, PreguntaService, PreguntasCheckService, \
    PreguntasCheckByUserAndEval, CantidadPreguntasByEvaluationId, PreguntaDeleteArrayIdService
from postgrado.service.ItemService import ItemsService, ItemService, ItemDeleteArrayIdService
from postgrado.service.FacultadService import FacultadService, FacultadServiceByOne, FacultadSaveUpdateService, FacultadDeleteService
from postgrado.service.SelectionMultipleService import ItemSelectionMultipleByIdPregunt
from postgrado.service.UserService import UserByFacultadIdService, UserSaveUpdateService, UserDeleteService, \
    UserByUsernameService

app = Flask(__name__)
api = Api(app)
CORS(app, resources={r"/api/*": {"origins": "*"}})

#Security
api.add_resource(Login, '/security')
api.add_resource(FacultyByUsername, '/security/faculty/<username>')
api.add_resource(TokenSession, '/security/activeSession/<usu_id>')
api.add_resource(DatetimeNowTokenUser, '/security/updateDatetimeToken/<usu_id>')

# Rol of Actor
api.add_resource(ActorService, '/actor/<id_user>')

api.add_resource(InstrumentsService, '/instruments')
api.add_resource(InstrumentSaveService, '/instrument/save')
api.add_resource(InstrumentUpdateService, '/instrument/update')
api.add_resource(InstrumentService, '/instrument/<code>')
api.add_resource(InstrumentsServiceByFacultyId, '/instrument/faculty/<facultyId>')
api.add_resource(DataActorEvaluation, '/instruments/sendDataEvaluation')
api.add_resource(GetDataActorEvaluation, '/instruments/sendDataEvaluation/<username>')
api.add_resource(DataActorEvaluationCheck, '/instruments/DataEvaluationCheck')
api.add_resource(DeleteInstrumentService, '/instrument/delete/<int_id>') #Elimina el Intrumento -> Evaluacion -> Preguntas -> Subpreguntas.
api.add_resource(InstrumentsServiceById, '/instrument/id/<int_id>')

api.add_resource(EvaluacionsService, '/evaluacions')
api.add_resource(EvaluacionUpdateService, '/evaluacion/update')
api.add_resource(EvaluacionService, '/evaluacion/<code_int>')
api.add_resource(EvaluacionCheckService, '/evaluacionscheck')
api.add_resource(EvaluacionCheckByUsernameService, '/evaluacionscheckcode/<username>')
api.add_resource(EvaluationCheckByFilterService, '/evaluacions/check/filter/<dataFilter>')

api.add_resource(PreguntasService, '/preguntas')
api.add_resource(PreguntaService, '/pregunta/<id_int>')
api.add_resource(PreguntaDeleteArrayIdService, '/pregunta/delete')
api.add_resource(PreguntasCheckService, '/pregunta/check/<username>')
api.add_resource(PreguntasCheckByUserAndEval, '/preguntas/usernameandcode')
api.add_resource(CantidadPreguntasByEvaluationId, '/preguntas/evaluation/<id_evaluation>')

api.add_resource(ItemsService, '/items')
api.add_resource(ItemDeleteArrayIdService, '/item/delete')
api.add_resource(ItemService, '/item/<pre_id>')

api.add_resource(ItemSelectionMultipleByIdPregunt, '/selection_multiple/items/<id_pre>')

api.add_resource(CombosService, '/combos')
api.add_resource(ComboServiceId, '/combo/<id>')
api.add_resource(ComboServicePost, '/combo')
api.add_resource(ComboService, '/combo/findByname/<name_combo>')

api.add_resource(ItemComboService, '/items-combo')
api.add_resource(ItemsComboService, '/items-combo/<id_combo>')

api.add_resource(FacultadService, '/facultad/findallfacultad')
api.add_resource(FacultadServiceByOne, '/facultad/findonebyid/<id_facultad>')
api.add_resource(FacultadSaveUpdateService, '/facultad/saveupdatefacultad')
api.add_resource(FacultadDeleteService, '/facultad/deletefacultad')
api.add_resource(ProgramByFacultadService, '/program/programsbyfacultadid/<id_facultad>')
api.add_resource(ProgramSaveUpdateService, '/program/saveupdateprogram')
api.add_resource(ProgramDeleteService, '/program/deleteprogram')
api.add_resource(ProgramServiceByOne, '/program/findonebyid/<pro_id>')

api.add_resource(UserByFacultadIdService, '/user/usersbyfaculty/<id_facultad>')
api.add_resource(UserByUsernameService, '/user/usersbyusername/<username>')
api.add_resource(UserSaveUpdateService, '/user/saveupdateuser')
api.add_resource(UserDeleteService, '/user/deleteuser')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
